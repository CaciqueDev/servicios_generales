<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('trabajos');

        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string('cedula_solicitante')->nullable();
            $table->string('nombre_solicitante')->nullable();
            $table->string('departamento')->nullable();
            $table->string('area')->nullable();
            $table->string('planta')->nullable();
            $table->string('tipo_orden')->nullable();
            $table->string('tipo_solicitud')->nullable();
            $table->string('descripcion_solicitud')->nullable();
            $table->string('prioridad')->nullable();
            $table->string('foto')->nullable();
            $table->string('ubicacion')->nullable();
            $table->string('cedula_tecnico')->nullable();
            $table->string('nombre_tecnico')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    public $cedula,$nombres,$departamento,$planta,$tipo_orden,$tipo_solicitud,$descripcion_solicitud,$prioridad,$foto,$ubicacion,$area;

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
