<?php

use App\Http\Livewire\UserComponent;

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\TicketComponent;
use App\Http\Livewire\DashboardComponent;
use App\Http\Livewire\Show\TicketShowComponent;
use App\Http\Livewire\Index\TicketIndexComponent;

Route::get('/entrar', function () {
    return view('auth.login');
})->name('entrar');

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');


Route::middleware(['auth:sanctum', 'verified'])->group( function () {
    Route::get('/dashboard',DashboardComponent::class)->name('dashboard');
    Route::get('/usuarios',UserComponent::class)->name('usuarios');
    Route::get('/tickets',TicketIndexComponent::class)->name('tickets');
    Route::get('/tickets/{ticket}',TicketShowComponent::class)->name('tickets-show');
});

Route::get('/',TicketComponent::class)->name('crear-ticket');