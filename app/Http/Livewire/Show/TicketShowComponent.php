<?php

namespace App\Http\Livewire\Show;

use App\Models\Ticket;
use Livewire\Component;
use App\Models\Personal;
use Illuminate\Validation\Rule;
use App\Http\Traits\MessageBannerTrait;

class TicketShowComponent extends Component
{

    use MessageBannerTrait;
    
    public $data,$cedula,$departamento,$area,$nombres,$observacion_realizado;

    public $disabledButtonSave = true;
    public $buttonResetField = false;

    public function mount($ticket)
    {
        $this->data = Ticket::where('ticket_number',$ticket)->first();
    }

    protected function rules()
    {
        return [
            'cedula' => ['required', 'min:7','max:8', 
            Rule::exists('App\Models\Personal')->where(function ($query) {
                return $query->where('departamento','like','%generales%');
            }),],
        ];
    }

    protected function messages()
    {
        return [
            'cedula.exists' => 'Está cédula no pertenece a ningún trabajador de Servicios Generales'
        ];
    }

    public function render()
    {
        return view('livewire.show.ticket-show-component',['ticket' => $this->data]);
    }

    public function resetField()
    {
        $this->reset(['departamento','nombres','cedula']);
        $this->disabledButtonSave = true;
        $this->buttonResetField = false;
    }

    public function searchingEmploye()
    {
        $this->validate();

        try {
            $query = Personal::where('cedula',$this->cedula)->first();

            $this->nombres = $query->nombre;
            $this->departamento = $query->departamento;
            $this->area = $query->area;
    
            $this->disabledButtonSave = false;
            $this->buttonResetField = true;

        } catch (\Exception $e) {
            $this->flash($e->getMessage(),'danger');

		    return $this->redirect('/usuarios');
        }
    }

    public function updateTicket()
    {
        $update = $this->data->update([
            'cedula_tecnico' => $this->cedula,
            'nombre_tecnico' => $this->nombres,
            'observacion_realizado' => $this->observacion_realizado,
            'status' => 1
        ]);

        if($update){
            $this->flash('Ticket actualizado correctamente.','success');

		    return $this->redirect('/tickets');
        }
    }
}
