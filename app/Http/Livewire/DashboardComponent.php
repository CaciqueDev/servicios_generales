<?php

namespace App\Http\Livewire;

use App\Models\Ticket;
use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Asantibanez\LivewireCharts\Facades\LivewireCharts;
use Asantibanez\LivewireCharts\Models\ColumnChartModel;

class DashboardComponent extends Component
{
    public $firstRun = true;
    public $showDataLabels = false;

    public $colors = [
        '#3CDC79',
        '#F0E512',
        '#E91313',
        '#66DA26',
        '#1340E9',
    ];

    public function render()
    {

        $querytipoSolicitud = Ticket::select('tipo_solicitud',DB::raw('count(*) as total'))->groupBy('tipo_solicitud')->whereMonth('created_at', date('m'))->get();
        $pieChartModel = $querytipoSolicitud
        ->reduce(function ($pieChartModel, $data) {
            $type = $data->tipo_solicitud;
            $value = $data->total;

            return $pieChartModel->addSlice($type, $value, array_rand($this->colors,1));
        }, LivewireCharts::pieChartModel()
            ->setTitle('Total - Tipos de solicitud')
            ->setAnimated($this->firstRun)
            ->withOnSliceClickEvent('onSliceClick')
            ->legendPositionBottom()
            ->legendHorizontallyAlignedCenter()
            ->setDataLabelsEnabled($this->showDataLabels)
            ->setColors($this->colors)
        );

        $resueltos = Ticket::where('status',1);
        $sinResolver = Ticket::where('status',0);

        $columnChartModel = 
        (new ColumnChartModel())
            ->setTitle('Total - Ticket resueltos y no resueltos')
            ->addColumn('Resueltos', $resueltos->count(), '#f6ad55')
            ->addColumn('Sin Resolver', $sinResolver->count(), '#fc8181')
        ;

        return view('livewire.dashboard-component',['columnChartModel' => $columnChartModel,'pieChartModel' => $pieChartModel]);
    }
}
