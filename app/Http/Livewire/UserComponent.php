<?php

namespace App\Http\Livewire;

use DB;
use App\Models\User;
use Livewire\Component;
use App\Models\Personal;
use Illuminate\Support\Facades\Hash;
use App\Http\Traits\MessageBannerTrait;

class UserComponent extends Component
{
    use MessageBannerTrait;

    public $area,$cedula,$departamento,$nombres,$password,$password_confirmation,$username,$email;

    public $disabledButtonSave = true;
    public $buttonResetField = false;


    public function render()
    {
        return view('livewire.user-component');
    }

    public function storeUsuario()
    {
        // dd($this->cedula,$this->email,$this->area,$this->username,$this->departamento);
//         $this->validate([
//             'cedula' => 'required|min:7|max:8|unique:users',
//             'email' => 'unique:users|required',
//             'username' => 'required|unique:users',
//             'area' => 'required',
//             'departamento', 'required',
//             // 'password' => 'required|confirmed|min:6',
//         ]);
// dd($this->departamento);
        $user = User::create([
            'cedula' => $this->cedula,
            'name' => $this->nombres,
            'email' => $this->email,
            'departamento' => $this->departamento,
            'username' => $this->username,
            'area' => $this->area,
            'password' => Hash::make($this->password),
            'clave' => $this->password
        ]);

        if($user)
        {
            $this->redirectFlashMessage('Se ha guardado correctamente','success','usuarios');
        }else{
            $this->redirectFlashMessage('¡Error!','danger','usuarios');
        }
    }

    public function redirectFlashMessage($message,$typeAlert,$route)
    {
        $this->flash($message,$typeAlert);

		return $this->redirect($route);
    }

    public function resetField()
    {
        $this->reset(['departamento','nombres','cedula','username','area']);
        $this->disabledButtonSave = true;
        $this->buttonResetField = false;
    }

    public function searchingEmploye()
    {
        $this->validate([
            'cedula' => 'required|min:7|max:8|exists:App\Models\Personal,cedula',
        ]);

        try {
            
            $query = Personal::where('cedula',$this->cedula)->first();

            $this->nombres = $query->nombre;
            $this->departamento = $query->departamento;
    
            $this->disabledButtonSave = false;
            $this->buttonResetField = true;

        } catch (\Exception $e) {
            $this->flash($e->getMessage(),'danger');

		    return $this->redirect('/usuarios');
        }
    }
}
