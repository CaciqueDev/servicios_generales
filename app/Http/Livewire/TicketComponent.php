<?php

namespace App\Http\Livewire;

use DB;
use App\Models\Ticket;
use Livewire\Component;
use App\Models\Personal;
use App\Http\Traits\MessageBannerTrait;

class TicketComponent extends Component
{
    use MessageBannerTrait;

    public $cedula,$nombres,$departamento,$planta,$tipo_orden,$tipo_solicitud,$descripcion_solicitud,$prioridad,$ubicacion,$area,$status,$observacion_realizado,$nombre_tecnico;
    public $ticket;
    public $sectionticketInfo = false;
    public $disabledButtonSave = true;
    public $buttonResetField = false;
    public $searchInfoTicket = false;

    public function render()
    {
        return view('livewire.ticket-component')->layout('layouts.guest');
    }

    public function redirectFlashMessage($message,$typeAlert,$route)
    {
        $this->flash($message,$typeAlert);

		return $this->redirect($route);
    }

    public function openModalSeacrhTicket()
    {
        $this->searchInfoTicket = true;
    }

    public function resetField()
    {
        $this->reset(['departamento','nombres','cedula']);
        $this->disabledButtonSave = true;
        $this->buttonResetField = false;
    }

    public function searchingEmploye()
    {
        $this->validate([
            'cedula' => 'required|min:7|max:8|exists:App\Models\Personal,cedula',
        ]);
        try {
            $query = Personal::where('cedula',$this->cedula)->first();

            $this->nombres = $query->nombre;
            $this->departamento = $query->departamento;
            $this->area = $query->area;
    
            $this->disabledButtonSave = false;
            $this->buttonResetField = true;

        } catch (\Exception $e) {
            $this->flash($e->getMessage(),'danger');

		    return $this->redirect('/');
        }
    }

    public function searchTicket()
    {
        $this->validate([
            'ticket' => 'required|exists:App\Models\Ticket,ticket_number',
        ]);
        $ticket = Ticket::where('ticket_number',$this->ticket)->first();
        $this->nombres = $ticket->nombre_solicitante;
        $this->tipo_orden = $ticket->tipo_orden;
        $this->tipo_solicitud = $ticket->tipo_solicitud;
        $this->descripcion_solicitud = $ticket->descripcion_solicitud;
        $this->status = $ticket->status;
        $this->nombre_tecnico = $ticket->nombre_tecnico;
        $this->observacion_realizado = $ticket->observacion_realizado;
        
        $this->sectionticketInfo = true;

      
    }

    public function storeTicket()
    {
        $this->validate([
            'cedula' => 'required|min:7|max:8',
            'departamento' => 'required',
            'area' => 'required',
            'planta' => 'required',
            'tipo_orden' => 'required',
            'tipo_solicitud' => 'required',
            'descripcion_solicitud' => 'required',
            'prioridad' => 'required',
            'ubicacion' => 'required',
        ]);

        $ticket = Ticket::create([
            'ticket_number' => mt_rand(0000,9999),
            'cedula_solicitante' => $this->cedula,
            'nombre_solicitante' => $this->nombres,
            'departamento' => $this->departamento,
            'area' => $this->area,
            'planta' => $this->planta,
            'tipo_orden' => $this->tipo_orden,
            'tipo_solicitud' => $this->tipo_solicitud,
            'descripcion_solicitud' => $this->descripcion_solicitud,
            'prioridad' => $this->prioridad,
            'ubicacion' => $this->ubicacion,
    ]);

        if($ticket)
        {
            flash()->overlay('ANOTE EL NÚMERO DE ESTE TICKET', 'Tu ticket es :#'.$ticket->ticket_number);
            return $this->redirect('/');
        }else{
            flash()->overlay('Error', 'Ocurrio un error inesperado');
            return $this->redirect('/');
        }
    }
}
