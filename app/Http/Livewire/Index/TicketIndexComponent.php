<?php

namespace App\Http\Livewire\Index;

use App\Models\Ticket;
use Livewire\Component;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class TicketIndexComponent extends LivewireDatatable
{
    public $model = Ticket::class;

    public function columns()
    {
        return [

            Column::callback(['id', 'name'], function ($id, $name) {
                return view('table-actions', ['id' => $id, 'name' => $name]);
            })
        ];
    }
    
    public function render()
    {
        return view('livewire.index.ticket-index-component');
    }
}
