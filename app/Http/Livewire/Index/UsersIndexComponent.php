<?php

namespace App\Http\Livewire\Index;

use Livewire\Component;

class UsersIndexComponent extends Component
{
    public function render()
    {
        return view('livewire.index.users-index-component');
    }
}
