<?php

namespace App\Http\Livewire\Datatable;

use App\Models\Ticket;
use Livewire\Component;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\BooleanColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class TicketsComponent extends LivewireDatatable
{
    public $model = Ticket::class;
    public $exportable = true;
    public $hideable = 'select';

    public function columns()
    {
        return [

            Column::name('ticket_number')->label('Nro. ticket')->searchable(),
            Column::name('nombre_solicitante')->searchable(),
            Column::name('departamento')->searchable(),
            Column::name('area')->searchable(),
            Column::name('tipo_orden')->filterable(['Correctiva', 'Preventiva', 'Inspeccion', 'Mejora']),
            Column::name('prioridad')->filterable(['Alta', 'Baja'])->hide(),
            Column::name('ubicacion')->searchable(),
            BooleanColumn::name('status')->filterable(),

            Column::callback(['ticket_number'], function ($ticket) {
                return view('actions.ticket', ['ticket' => $ticket]);
            })

        ];
    }
}
