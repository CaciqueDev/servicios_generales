<?php

namespace App\Http\Traits;

trait MessageBannerTrait {

    public function flash(string $message, string $style = 'success')
    {
        request()->session()->flash('flash.banner', $message);
        request()->session()->flash('flash.bannerStyle', $style);
    }

}