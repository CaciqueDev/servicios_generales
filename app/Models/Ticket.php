<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;

    public $fillable = [
            'ticket_number',
            'cedula_solicitante',
            'nombre_solicitante',
            'departamento',
            'area',
            'planta',
            'tipo_orden',
            'tipo_solicitud',
            'descripcion_solicitud',
            'prioridad',
            'foto',
            'ubicacion',
            'cedula_tecnico',
            'nombre_tecnico',
            'observacion_realizado',
            'status'
    ];
}
