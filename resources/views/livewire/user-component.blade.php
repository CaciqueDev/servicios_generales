<div x-data="{ buttonSave : @entangle('disabledButtonSave'), buttonReset : @entangle('buttonResetField') }">
    <x-jet-form-section submit="storeUsuario" class="p-6">
        <x-slot name="title">
            {{ __('Crear Usuario') }}
        </x-slot>

        <x-slot name="description">
            {{ __('Debe llenar todos los datos.') }}<br>
        </x-slot>

        <x-slot name="form">
            <div class="col-span-12 md:col-span-6 sm:col-span-6">
                <x-jet-label value="{{ __('Nro. Documento') }}" />
                <x-jet-input type="number" class="mt-1 block w-full" x-bind:readonly="buttonReset" x-bind:class="{ 'bg-gray-200' : buttonReset }" wire:keydown.shift="searchingEmploye" wire:model.defer="cedula" autocomplete="on" />
                <small>Presione <strong>Shift</strong> para buscar empleado</small>
                <x-jet-input-error for="cedula" class="mt-2" />
            </div>
            <div class="col-span-12 sm:col-span-12">
                <x-jet-label for="nombres" value="{{ __('Nombres') }}" />
                <x-jet-input required type="text" id="nombres" class="mt-1 block w-full bg-gray-200" readonly wire:model.defer="nombres"/>
                <x-jet-input-error for="nombres" class="mt-2" />
                <small wire:loading wire:target="searchingEmploye">Cargando...</small>
            </div>
            <div class="col-span-12 sm:col-span-12">
                <x-jet-label value="{{ __('Departamento') }}" />
                <x-jet-input required type="text" class="mt-1 block w-full bg-gray-200" wire:model.defer="departamento" readonly/>
                <x-jet-input-error for="departamento" class="mt-2" />
                <small wire:loading wire:target="searchingEmploye">Cargando...</small>
            </div>
            <hr class="col-span-12 sm:col-span-12">
            <div class="col-span-12 md:col-span-6 ">
                <x-jet-label value="{{ __('Email') }}" />
                <x-jet-input type="email" class="mt-1 block w-full" wire:model.defer="email" required />
                <x-jet-input-error for="email" class="mt-2" />
            </div>
            <div class="col-span-12 md:col-span-6 ">
                <x-jet-label value="{{ __('Usuario') }}" />
                <x-jet-input type="text" class="mt-1 block w-full" wire:model.defer="username" required />
                <x-jet-input-error for="username" class="mt-2" />
            </div>
            <div class="col-span-12 sm:col-span-12">
                <x-jet-label  value="{{ __('Area') }}" />
                <select wire:model.defer="area" required class="mt-1 block w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm">
                    <option value="">Seleccione...</option>
                    <option value="Reparaciones Menores">Reparaciones Menores</option>
                    <option value="Electricidad">Electricidad</option>
                    <option value="Refrigeracion">Refiregarión</option>
                    <option value="Mantenimiento General">Mantenimiento General</option>
                    <option value="Areas Verdes">Areas Verdes</option>
                    <i class="ion-playstation"></i>
                </select>
                <x-jet-input-error for="area" class="mt-2" />
            </div>
            <div class="col-span-6">
                <x-jet-label value="{{ __('Contraseña') }}" />
                <x-jet-input type="password" class="mt-1 block w-full" wire:model.defer="password" />
                <x-jet-input-error for="password" class="mt-2" />
            </div>
            <div class="col-span-6">
                <x-jet-label for="password_confirmation" value="{{ __('Repetir Contraseña') }}" />
                <x-jet-input required type="password" id="nombres" class="mt-1 block w-full" wire:model.defer="password_confirmation"/>
                <x-jet-input-error for="password_confirmation" class="mt-2" />
            </div>
        </x-slot>

        <x-slot name="actions">
            <x-jet-danger-button x-show="buttonReset" wire:click="resetField">
                {{ __('Reiniciar Datos') }}
            </x-jet-danger-button>

            <x-jet-button x-bind:disabled="buttonSave">
                {{ __('Guardar') }}
            </x-jet-button>
        </x-slot>
    </x-jet-form-section>
</div>
