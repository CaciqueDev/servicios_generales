<div>
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
        <div class="mt-8 p-5 bg-gray-200 dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
            <livewire:datatable.tickets-component>
        </div>

    </div>
</div>
