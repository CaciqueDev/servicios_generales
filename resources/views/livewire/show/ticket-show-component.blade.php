<div x-data="{ buttonSave : @entangle('disabledButtonSave'), buttonReset : @entangle('buttonResetField') }">
    <livewire:flash-container />
    <x-jet-form-section submit="updateTicket" class="p-6">
        <x-slot name="title">
            {{ __('Ticket #'.$ticket->ticket_number) }}
        </x-slot>

        <x-slot name="description">
            {{ __('Asignar o ver la información de este ticket.') }}<br>
        </x-slot>

        <x-slot name="form">
            <div class="col-span-12">
                <x-jet-label value="{{ __('Ubicación Exacta') }}" />
                <span class="font-bold text-lg">{{ $ticket->ubicacion }}</span>
            </div>
            <div class="col-span-12">
                <x-jet-label  value="{{ __('Prioridad') }}" />
                <span class="font-bold text-lg">{{ $ticket->prioridad }}</span>
            </div>
            <div class="col-span-12 md:col-span-4">
                <x-jet-label  value="{{ __('Planta') }}" />
                <span class="font-bold text-lg">{{ $ticket->planta }}</span>
            </div>
            <div class="col-span-12 md:col-span-4">
                <x-jet-label  value="{{ __('Tipo de orden') }}" />
                <span class="font-bold text-lg">{{ $ticket->tipo_orden }}</span>
            </div>
            <div class="col-span-12 md:col-span-4">
                <x-jet-label  value="{{ __('Tipo de la solicitud') }}" />
                <span class="font-bold text-lg">{{ $ticket->tipo_solicitud }}</span>
            </div>
            <div class="col-span-12">
                <x-jet-label  value="{{ __('Descripción de la solicitud') }}" />
                <span class="font-bold text-lg">{{ $ticket->descripcion_solicitud }}</span>
            </div>
            @if($ticket->status == 1)
                <hr class="col-span-12 sm:col-span-12">
                <div class="col-span-12">
                    <span class="text-center text-lg font-bold mb-2">Esté ticket está resuelto</span>
                </div>
                <div class="col-span-12">
                    <x-jet-label value="{{ __('Cédula del trabajador') }}" />
                    <span class="font-bold text-lg">{{ $ticket->cedula_tecnico }}</span>
                </div>
                <div class="col-span-12">
                    <x-jet-label  value="{{ __('Nombre del trabajador') }}" />
                    <span class="font-bold text-lg">{{ $ticket->nombre_tecnico }}</span>
                </div>
                <div class="col-span-12">
                    <x-jet-label  value="{{ __('Observación') }}" />
                    <span class="font-bold text-lg">{{ $ticket->observacion_realizado }}</span>
                </div>
            @endif
            
            <hr class="col-span-12 sm:col-span-12">
            <div class="col-span-12">
                @if($ticket->status == 1)
                    <span class="text-center text-lg font-bold mb-2">Cambiar al Supervisor que realizo este ticket</span>
                @else
                    <span class="text-center text-lg font-bold mb-2">Seleccionar al Supervisor que ejecuto la petición de este ticket</span>
                @endif
            </div>
            <div class="col-span-12 md:col-span-6 sm:col-span-6">
                <x-jet-label value="{{ __('Cedula del trabajador que realizo esta tarea') }}" />
                <x-jet-input type="number" class="mt-1 block w-full" x-bind:readonly="buttonReset" x-bind:class="{ 'bg-gray-200' : buttonReset }" wire:model.defer="cedula" wire:keydown.shift="searchingEmploye" autocomplete="on" />
                <small>Presione <strong>Tecla Espacio o el botón "Buscar"</strong> para buscar empleado</small>
                <x-jet-input-error for="cedula" class="mt-2" />
            </div>
            <div class="col-span-12 md:col-span-6 sm:col-span-6">
                <x-jet-label value="{{ __('Presione para buscar') }}" />
                <button type="button" :disabled="buttonReset" wire:click="searchingEmploye" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150 mt-2">
                    Buscar
                </button>
            </div>
            <div class="col-span-12 sm:col-span-12">
                <x-jet-label for="nombres" value="{{ __('Nombres') }}" />
                <x-jet-input required type="text" id="nombres" class="mt-1 block w-full bg-gray-200" readonly wire:model.defer="nombres" autocomplete="on" />
                <x-jet-input-error for="nombres" class="mt-2" />
                <small wire:loading wire:target="searchingEmploye">Cargando...</small>
            </div>
            <div class="col-span-12 sm:col-span-12">
                <x-jet-label value="{{ __('Departamento') }}" />
                <x-jet-input required type="text" class="mt-1 block w-full bg-gray-200" wire:model.defer="departamento" readonly autocomplete="on" />
                <x-jet-input-error for="departamento" class="mt-2" />
                <small wire:loading wire:target="searchingEmploye">Cargando...</small>
            </div>
            <div class="col-span-12">
                <x-jet-label  value="{{ __('Observación del trabajo') }}" />
                <textarea required wire:model.defer="observacion_realizado" class="border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm w-full h-32"></textarea>
                <x-jet-input-error for="observacion_realizado" class="mt-2" />
            </div>
            
        </x-slot>

        <x-slot name="actions">
            <x-jet-danger-button x-show="buttonReset" wire:click="resetField">
                {{ __('Buscar otro empleado') }}
            </x-jet-danger-button>

            <x-jet-button x-bind:disabled="buttonSave">
                {{ __('Resolver ticket') }}
            </x-jet-button>
        </x-slot>
    </x-jet-form-section>
</div>
