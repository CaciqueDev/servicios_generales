<div x-data="{ buttonSave : @entangle('disabledButtonSave'), buttonReset : @entangle('buttonResetField') }">
    <div class="flex items-center justify-center">
        <x-jet-authentication-card-logo />
    </div>
    <livewire:flash-container />
    <x-jet-form-section submit="storeTicket" class="p-4">
        <x-slot name="title">
            <p class="fotn-bold text-black">{{  __('Crear Ticket') }}</p>
        </x-slot>

        <x-slot name="description">
            <p class="fotn-bold text-black">{{ __('Debe llenar todos los campos.') }}<br></p>
            <div class="flex gap-3">
                <a href="{{ route('entrar') }}" class="inline-flex items-center px-4 mb-8 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150 mt-2">
                    ENTRAR AL PANEL
                </a>
                <button type="button" wire:click="openModalSeacrhTicket" class="inline-flex items-center px-4 mb-8 py-2 bg-green-600 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-green-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150 mt-2">
                    BUSCAR TICKET
                </button>
            </div>
            <div class="overflow-hidden bg-white rounded max-w-xs w-full shadow-lg  mt-4 leading-normal">
                <div class="block group hover:bg-blue-200 p-4 border-b">
                    <p class="font-bold text-lg mb-1 text-black ">Paso 1 : Busca tu cédula</p>
                    <p class="text-grey-darker mb-2 ">Debes buscar tu cédula para buscar llenar tus datos.</p>
                </div>
                <div class="block group hover:bg-blue-200 p-4">
                    <p class="font-bold text-lg mb-1 text-black ">Paso 2: llenar la ubicación exacta del lugar donde esta la falla.</p>
                    <p class="text-grey-darker mb-2 ">Debe llenar este campo y poner exactamente el sitio donde quiere que se haga la reparación.</p>
                </div>
                <div class="block group hover:bg-blue-200 p-4">
                    <p class="font-bold text-lg mb-1 text-black ">Paso 3: Llenar campos restantes</p>
                    <p class="text-grey-darker mb-2 ">Seleciconar prioridad, planta, tipo de orden y el tipo de solicitud.</p>
                </div>
                <div class="block group hover:bg-blue-200 p-4">
                    <p class="font-bold text-lg mb-1 text-black ">Paso 4: llenar la observación de la falla</p>
                    <p class="text-grey-darker mb-2 ">Este campo es importante porque se debe detallar el fallo en cuestión.</p>
                </div>
                <div class="block group hover:bg-blue-200 p-4">
                    <p class="font-bold text-lg mb-1 text-black ">OJO : cualquier fallo o duda</p>
                    <p class="text-grey-darker mb-2 ">Llamar al departamento de tecnología o al departamento de servicios generales.</p>
                </div>
            </div>
        </x-slot>

        <x-slot name="form">
            <div class="col-span-12 md:col-span-6 sm:col-span-6">
                <x-jet-label value="{{ __('Tu número de cédula') }}" />
                <x-jet-input type="number" class="mt-1 block w-full" x-bind:readonly="buttonReset" x-bind:class="{ 'bg-gray-200' : buttonReset }" wire:model.defer="cedula" wire:keydown.shift="searchingEmploye" autocomplete="on" />
                <small>Presione <strong>Tecla Shift o el botón "Buscar"</strong> para buscar tus datos</small>
                <x-jet-input-error for="cedula" class="mt-2" />
            </div>
            <div class="col-span-12 md:col-span-6 sm:col-span-6">
                <x-jet-label value="{{ __('Presione para buscar') }}" />
                <button type="button" :disabled="buttonReset" wire:click="searchingEmploye" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150 mt-2">
                    Buscar
                </button>
            </div>
            <div class="col-span-12 sm:col-span-12">
                <x-jet-label for="nombres" value="{{ __('Nombres') }}" />
                <x-jet-input required type="text" id="nombres" class="mt-1 block w-full bg-gray-200" readonly wire:model.defer="nombres" autocomplete="on" />
                <x-jet-input-error for="nombres" class="mt-2" />
                <small wire:loading wire:target="searchingEmploye">Cargando...</small>
            </div>
            <div class="col-span-12 sm:col-span-12">
                <x-jet-label value="{{ __('Departamento') }}" />
                <x-jet-input required type="text" class="mt-1 block w-full bg-gray-200" wire:model.defer="departamento" readonly autocomplete="on" />
                <x-jet-input-error for="departamento" class="mt-2" />
                <small wire:loading wire:target="searchingEmploye">Cargando...</small>
            </div>
            <hr class="col-span-12 sm:col-span-12">
            <div class="col-span-12">
                <x-jet-label value="{{ __('Ubicación Exacta') }}" />
                <x-jet-input type="text" class="mt-1 block w-full" wire:model.defer="ubicacion" required />
                <x-jet-input-error for="ubicacion" class="mt-2" />
            </div>
            <div class="col-span-12">
                <x-jet-label  value="{{ __('Prioridad') }}" />
                <select wire:model.defer="prioridad" required class="mt-1 block w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm">
                    <option value="">Seleccione...</option>
                    <option value="Alta">Alta</option>
                    <option value="Baja">Baja</option>
                </select>
                <x-jet-input-error for="prioridad" class="mt-2" />
            </div>
            <div class="col-span-12 md:col-span-4">
                <x-jet-label  value="{{ __('Planta') }}" />
                <select wire:model.defer="planta" required class="mt-1 block w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm">
                    <option value="">Seleccione...</option>
                    <option value="Planta">Planta - Kimberly Clark</option>
                    <option value="CERCOF">CERCOF</option>
                    <option value="CEDISMAR">CEDISMAR</option>
                </select>
                <x-jet-input-error for="planta" class="mt-2" />
            </div>
            <div class="col-span-12 md:col-span-4">
                <x-jet-label  value="{{ __('Tipo de orden') }}" />
                <select wire:model.defer="tipo_orden" required class="mt-1 block w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm">
                    <option value="">Seleccione...</option>
                    <option value="Correctiva">Correctiva</option>
                    <option value="Preventiva">Preventiva</option>
                    <option value="Inspeccion">Inspección</option>
                    <option value="Mejora">Mejora</option>
                </select>
                <x-jet-input-error for="tipo_orden" class="mt-2" />
            </div>
            <div class="col-span-12 md:col-span-4">
                <x-jet-label  value="{{ __('Tipo de la solicitud') }}" />
                <select wire:model.defer="tipo_solicitud" required class="mt-1 block w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm">
                    <option value="">Seleccione...</option>
                    <option value="Reparaciones Menores">Reparaciones Menores</option>
                    <option value="Electricidad">Electricidad</option>
                    <option value="Refrigeracion">Refiregación</option>
                    <option value="Mantenimiento General">Mantenimiento General</option>
                    <option value="Areas Verdes">Areas Verdes</option>
                    <i class="ion-playstation"></i>
                </select>
                <x-jet-input-error for="tipo_solicitud" class="mt-2" />
            </div>
            <div class="col-span-12">
                <x-jet-label  value="{{ __('Descripción de la solicitud') }}" />
                <textarea wire:model.defer="descripcion_solicitud" class="border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm w-full h-32"></textarea>
                <x-jet-input-error for="descripcion_solicitud" class="mt-2" />
            </div>
        </x-slot>

        <x-slot name="actions">
            <x-jet-danger-button x-show="buttonReset" wire:click="resetField">
                {{ __('Buscar otra cédula') }}
            </x-jet-danger-button>

            <x-jet-button x-bind:disabled="buttonSave">
                {{ __('Guardar') }}
            </x-jet-button>
        </x-slot>
    </x-jet-form-section>

    <x-jet-dialog-modal wire:model="searchInfoTicket">
        <x-slot name="title">
            {{ __('Buscar ticket') }}
        </x-slot>

        <x-slot name="content">
            {{ __('Ponga el número de su ticket sin el "#" parea buscar la información de ese ticket.') }}

            <div class="mt-4" x-data="{}" x-on:confirming-delete-user.window="setTimeout(() => $refs.ticket.focus(), 150)">
                <x-jet-input type="number" class="mt-1 block w-3/4"
                            placeholder="{{ __('0000') }}"
                            x-ref="ticket"
                            wire:model.defer="ticket"
                            />

                <x-jet-input-error for="ticket" class="mt-2" />
                @if($sectionticketInfo)
                    <div class="grid grid-cols-12 gap-6 mt-8">
                        <div class="col-span-12">
                            <x-jet-label value="{{ __('Nombre Solicitante') }}" />
                            <span class="font-bold text-lg">{{ $nombres }}</span>
                        </div>
                        <div class="col-span-12 md:col-span-4">
                            <x-jet-label  value="{{ __('Tipo de orden') }}" />
                            <span class="font-bold text-lg">{{$tipo_orden }}</span>
                        </div>
                        <div class="col-span-12 md:col-span-4">
                            <x-jet-label  value="{{ __('Tipo de la solicitud') }}" />
                            <span class="font-bold text-lg">{{$tipo_solicitud }}</span>
                        </div>
                        <div class="col-span-12">
                            <x-jet-label  value="{{ __('Descripción de la solicitud') }}" />
                            <span class="font-bold text-lg">{{$descripcion_solicitud }}</span>
                        </div>
                        @if($status != 1)
                            <div class="col-span-12">
                                <span class="text-center text-lg font-bold mb-2 text-red-700">El Ticket no se ha resuelto</span>
                            </div>
                        @endif
                        @if($status == 1)
                            <hr class="col-span-12 sm:col-span-12">
                            <div class="col-span-12">
                                <span class="text-center text-lg font-bold mb-2 text-green-600">El ticket está resuelto</span>
                            </div>
                            <div class="col-span-12">
                                <x-jet-label  value="{{ __('Nombre del trabajador') }}" />
                                <span class="font-bold text-lg">{{ $nombre_tecnico }}</span>
                            </div>
                            <div class="col-span-12">
                                <x-jet-label  value="{{ __('Observación') }}" />
                                <span class="font-bold text-lg">{{ $observacion_realizado }}</span>
                            </div>
                        @endif
                    </div>
                @endif
            </div>
        </x-slot>

        <x-slot name="footer">
            <x-jet-secondary-button wire:click="$toggle('searchInfoTicket')" wire:loading.attr="disabled">
                {{ __('Cerrar') }}
            </x-jet-secondary-button>

            <x-jet-danger-button class="ml-2" wire:click="searchTicket" wire:loading.attr="disabled">
                {{ __('Buscar Ticket') }}
            </x-jet-danger-button>
        </x-slot>
    </x-jet-dialog-modal>
</div>
